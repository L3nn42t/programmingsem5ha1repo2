﻿using System.Collections.Generic;
using System.Linq;


namespace GP3._04_SearchAlgorithms
{
	public class AStarSearch : SearchBase
	{
		//Startup copied fron dijkstra 
		protected override void InitializeSearch()
		{
			_startNode = _startMarker.ClosestGridNode;
			_goalNode = _endMarker.ClosestGridNode;

			foreach (GridNode gridNode in _visited.Keys)
			{
				gridNode.Reset();
			}

			_openList = new List<GridNode>();
			_visited = new Dictionary<GridNode, GridNode>();

			_openList.Add(_startNode);

			_startNode.CostSoFar = 0;
		}


		protected override bool StepToGoal()
		{
			_openList = _openList.OrderBy(n => (n.Heuristic + n.CostSoFar)).ToList();
			GridNode current = _openList[0];

			// goal found
			if (current == _goalNode)
			{
				return true;
			}

			foreach (GridNode next in current.Neighbours)
			{
				if (next.IsWall)
				{
					continue;
				}

				float newCost = current.CostSoFar + next.Cost;
				
				if (_visited.ContainsKey(next))
				{
					//reevaluates old ones if cost is low enough
					if (newCost < next.CostSoFar)
					{
						next.CostSoFar = newCost;
						next.Heuristic = GetHeuristic(_goalNode, next);
						_visited[next] = current;
						_openList.Add(next);
						next.SetGridNodeSearchState(GridNodeSearchState.Queue);
					}					
				}
				// basic searching for unvisited nodes, like breath first search
				else if (!_visited.ContainsKey(next))
				{
					next.Heuristic = GetHeuristic(_goalNode, next);
					next.CostSoFar = newCost;
					_openList.Add(next);
					_visited.Add(next, current);
					next.SetGridNodeSearchState(GridNodeSearchState.Queue);
				}
	
			}

			_openList.Remove(current);
			current.SetGridNodeSearchState(GridNodeSearchState.Processed);
			// not yet finished
			return false;
		}
		private float GetHeuristic(GridNode goal, GridNode next)
		{
			return (goal.transform.position - next.transform.position).magnitude;
		}

	}
}